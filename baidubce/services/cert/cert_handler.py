import httplib
import json
from baidubce.exception import BceClientError
from baidubce.exception import BceServerError


def parse_error(http_response, response):
    """If the body is not empty, convert it to a python object and set as the value of
    response.body. http_response is always closed if no error occurs.

    :param http_response: the http_response object returned by HTTPConnection.getresponse()
    :type http_response: httplib.HTTPResponse

    :param response: general response object which will be returned to the caller
    :type response: baidubce.BceResponse

    :return: false if http status code is 2xx, raise an error otherwise
    :rtype bool

    :raise baidubce.exception.BceClientError: if http status code is NOT 2xx
    """
    if http_response.status / 100 == httplib.OK / 100:
        return False
    if http_response.status / 100 == httplib.CONTINUE / 100:
        raise BceClientError('Can not handle 1xx http status code')
    bse = None
    body = http_response.read()
    if body:
        d = json.loads(body)
        if 'code' in d:
            bse = BceServerError(d['message'], code=d['code'], request_id=d['requestId'])
        else:
            bse = BceServerError(d['Message'], code=d['Code'], request_id="")

    if bse is None:
        bse = BceServerError(http_response.reason, request_id=response.metadata.bce_request_id)
    bse.status_code = http_response.status
    raise bse
