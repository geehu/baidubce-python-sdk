# coding: utf-8


import copy
import json
import logging
import baidubce
import sys
import time
import traceback
import re

from urllib import quote
from baidubce import bce_base_client
from baidubce import utils
from baidubce.auth import bce_v1_signer
from baidubce.http import bce_http_client
from baidubce.http import handler
from baidubce.http import http_content_types
from baidubce.http import http_headers
from baidubce.http import http_methods
from baidubce.services.cert import cert_handler
from baidubce.exception import BceClientError
from baidubce.exception import BceServerError
from baidubce.utils import required

_logger = logging.getLogger(__name__)


class CertClient(bce_base_client.BceBaseClient):
    """
    CertClient
    目前该接口只支持HTTP协议，不能使用HTTPS
    """
    prefix = '/v1'

    def __init__(self, config=None):
        bce_base_client.BceBaseClient.__init__(self, config)

    def list_certificate(self, config=None):
        return self._send_request(
            http_methods.GET,
            '/certificate',
            body={},
            params={},
            config=config)

    def create_cert(self, cert_name, cert_server_data, cert_private_data, cert_link_data=None, config=None):
        data = {
            "certName": cert_name,
            "certServerData": cert_server_data,
            "certPrivateData": cert_private_data
        }
        if cert_link_data:
            data["certLinkData"] = cert_link_data
        return self._send_request(
            http_methods.POST,
            '/certificate',
            body=json.dumps(data),
            params={},
            config=config)

    @staticmethod
    def _merge_config(self, config):
        if config is None:
            return self.config
        else:
            new_config = copy.copy(self.config)
            new_config.merge_non_none_values(config)
            return new_config

    def _send_request(
            self, http_method, path,
            body=None, headers=None, params=None,
            config=None,
            body_parser=None, special=False):
        config = self._merge_config(self, config)
        if body_parser is None:
            body_parser = handler.parse_json
        headers = {}
        headers[http_headers.CONTENT_TYPE] = http_content_types.JSON
        return self.send_request(config, bce_v1_signer.sign,
                                 [cert_handler.parse_error, body_parser],
                                 http_method, CertClient.prefix + path,
                                 body, headers, params, special)

    def send_request(
            self,
            config,
            sign_function,
            response_handler_functions,
            http_method, path, body, headers, params, special=False):
        """
        Send request to BCE services.
        :param config
        :type config: baidubce.BceClientConfiguration
        :param sign_function:
        :param response_handler_functions:
        :type response_handler_functions: list
        :param request:
        :type request: baidubce.internal.InternalRequest
        :return:
        :rtype: baidubce.BceResponse
        """
        t = int(time.time())
        _logger.debug('%s request start: %s %s, %s, %s, %d',
                      http_method, path, headers, params, body, t)
        headers = headers or {}
        user_agent = 'bce-sdk-python/%s/%s/%s' % (
            baidubce.SDK_VERSION, sys.version, sys.platform)
        user_agent = user_agent.replace('\n', '')
        headers[http_headers.USER_AGENT] = user_agent
        should_get_new_date = False
        headers[http_headers.HOST] = config.endpoint
        if isinstance(body, unicode):
            body = body.encode(baidubce.DEFAULT_ENCODING)
        if not body:
            headers[http_headers.CONTENT_LENGTH] = 0
        elif isinstance(body, str):
            headers[http_headers.CONTENT_LENGTH] = len(body)
        elif http_headers.CONTENT_LENGTH not in headers:
            raise ValueError('No %s is specified.' % http_headers.CONTENT_LENGTH)
        offset = None
        if hasattr(body, "tell") and hasattr(body, "seek"):
            offset = body.tell()
        protocol, host, port = utils.parse_host_port(config.endpoint, config.protocol)
        path = quote(path)
        headers[http_headers.HOST] = host
        if port != config.protocol.default_port:
            headers[http_headers.HOST] += ':' + str(port)
        headers[http_headers.AUTHORIZATION] = sign_function(
            config.credentials, http_method, path, headers, params,
            headers_to_sign=["host", "content-type"])
        encoded_params = utils.get_canonical_querystring(params, False)
        if len(encoded_params) > 0:
            uri = path + '?' + encoded_params
        else:
            uri = path
            bce_http_client.check_headers(headers)
        retries_attempted = 0
        errors = []
        while True:
            conn = None
            try:
                if should_get_new_date is True:
                    headers[http_headers.BCE_DATE] = utils.get_canonical_time()

                headers[http_headers.AUTHORIZATION] = sign_function(
                    config.credentials, http_method, path, headers, params,
                    headers_to_sign=["host", "content-type"])
                if retries_attempted > 0 and offset is not None:
                    body.seek(offset)

                conn = bce_http_client._get_connection(protocol, host,
                                                       port, config.connection_timeout_in_mills)

                http_response = bce_http_client._send_http_request(
                    conn, http_method, uri, headers, body, config.send_buf_size)

                headers_list = http_response.getheaders()
                _logger.debug(
                    'request return: status=%d, headers=%s' % (http_response.status, headers_list))
                if special:
                    return http_response
                response = bce_http_client.BceResponse()
                response.set_metadata_from_headers(dict(headers_list))
                for handler_function in response_handler_functions:
                    if handler_function(http_response, response):
                        break
                return response
            except Exception as e:
                if conn is not None:
                    conn.close()
                # insert ">>>>" before all trace back lines and then save it
                errors.append('\n'.join('>>>>' + line for line in traceback.format_exc().
                                        splitlines()))
                if config.retry_policy.should_retry(e, retries_attempted):
                    delay_in_millis = config.retry_policy.get_delay_before_next_retry_in_millis(
                        e, retries_attempted)
                    time.sleep(delay_in_millis / 1000.0)
                else:
                    raise bce_http_client.BceHttpClientError('Unable to execute HTTP request. '
                                                             'Retried %d times. All trace backs:\n'
                                                             '%s' % (retries_attempted,
                                                                     '\n'.join(errors)), e)